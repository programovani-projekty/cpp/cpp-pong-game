## PONG GAME 
Jedná se zde o hru Pong game.

Na projektu spolupracovali:

- Pavel Pikola
- Martin May

Tento projekt trvalo vytvořit 4 hodiny a 43 minut.

*Hra je vytvořena v C++ s použitím knihovny [SFML](https://www.sfml-dev.org/index.php).*