#include <windows.h>
#include <iostream>
#include <string>
#include <math.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML\Audio.hpp>

#define PI 3.14159265

using namespace std;

int main()
{

    sf::Clock clocks;

    //definition of colors
    sf::Color color_ledge(169, 169, 169);
    sf::Color color_net(0, 0, 255);
    sf::Color color_racket(255, 255, 255);
    sf::Color black_opacity(0, 0, 0, 200);
    sf::Color black(0, 0, 0);
    sf::Color grey_buttons(180, 180, 180);
    sf::Color hover_grey(120, 120, 120);

    //loading font
    sf::Font font;
    if (!font.loadFromFile("BAUHS93.TTF"))
    {
        cout << "Error" << endl;
        return 0;
    }

    sf::SoundBuffer buffer;

    if (!buffer.loadFromFile("jump.wav"))
    {
        cout << "Error" << endl;
        return 0;
    }

    sf::Sound jumping;
    jumping.setBuffer(buffer);

    //mouse variable
    sf::Vector2i mouse_position;

    //drawing window
    sf::RenderWindow window(sf::VideoMode(1280, 960), "Pong Game by Pavel Pikola");

    window.setFramerateLimit(480);

    //opacity pause
    sf::RectangleShape dark_background(sf::Vector2f(1280.f, 960.f));
    dark_background.setFillColor(black_opacity);
    dark_background.setPosition(0.f, 0.f);

    //pause button
    sf::RectangleShape pause_button(sf::Vector2f(80.f, 80.f));
    pause_button.setFillColor(grey_buttons);
    pause_button.setPosition(10.f, 40.f);
    sf::FloatRect pause_button_r;
    pause_button_r = pause_button.getGlobalBounds();

    // 1/2 pause line
    sf::RectangleShape line_pause(sf::Vector2f(15.f, 60.f));
    line_pause.setFillColor(black);
    line_pause.setPosition(30.f, 50.f);

    // 2/2 pause line
    sf::RectangleShape line_two_pause(sf::Vector2f(15.f, 60.f));
    line_two_pause.setFillColor(black);
    line_two_pause.setPosition(55.f, 50.f);

    //button continue
    sf::RectangleShape b_continue(sf::Vector2f(500.f, 125.f));
    b_continue.setFillColor(grey_buttons);
    b_continue.setPosition(390.f, 250.f);
    sf::FloatRect b_continue_r;
    b_continue_r = b_continue.getGlobalBounds();

    sf::Text t_continue;
    t_continue.setFont(font);
    t_continue.setString("Continue");
    t_continue.setCharacterSize(90);
    t_continue.setFillColor(black);
    t_continue.setPosition(400.f, 250.f);
    sf::FloatRect t_continue_r;
    t_continue_r = t_continue.getGlobalBounds();
    t_continue.setPosition((1280.f - t_continue_r.width) / 2, 250.f);

    //button new game
    sf::RectangleShape b_new_game(sf::Vector2f(500.f, 125.f));
    b_new_game.setFillColor(grey_buttons);
    b_new_game.setPosition(390.f, 450.f);
    sf::FloatRect b_new_game_r;
    b_new_game_r = b_new_game.getGlobalBounds();

    sf::Text t_new_game;
    t_new_game.setFont(font);
    t_new_game.setString("New game");
    t_new_game.setCharacterSize(90);
    t_new_game.setFillColor(black);
    t_new_game.setPosition(420.f, 450.f);
    sf::FloatRect t_new_game_r;
    t_new_game_r = t_new_game.getGlobalBounds();
    t_new_game.setPosition((1280.f - t_new_game_r.width) / 2, 450.f);

    //button end
    sf::RectangleShape b_end(sf::Vector2f(500.f, 125.f));
    b_end.setFillColor(grey_buttons);
    b_end.setPosition(390.f, 650.f);
    sf::FloatRect b_end_r;
    b_end_r = b_end.getGlobalBounds();

    sf::Text t_end;
    t_end.setFont(font);
    t_end.setString("End");
    t_end.setCharacterSize(90);
    t_end.setFillColor(black);
    sf::FloatRect t_end_r;
    t_end_r = t_end.getGlobalBounds();
    t_end.setPosition((1280.f - t_end_r.width) / 2, 650.f);

    //top bar and color
    sf::RectangleShape top_bar(sf::Vector2f(1280.f, 30.f));
    top_bar.setFillColor(color_ledge);

    //bottom bar and color
    sf::RectangleShape bottom_bar(sf::Vector2f(1280.f, 30.f));
    bottom_bar.setFillColor(color_ledge);
    bottom_bar.setPosition(0.f, 930.f);

    //net, color and position
    sf::RectangleShape net(sf::Vector2f(10.f, 900.f));
    net.setFillColor(color_net);
    net.setPosition(635.f, 30.f);

    //left racket
    float ylr = 380;
    sf::RectangleShape left_racket(sf::Vector2f(20.f, 200.f));
    left_racket.setFillColor(color_racket);
    left_racket.setPosition(40.f, ylr);

    //right racket
    float yrr = 380;
    sf::RectangleShape right_racket(sf::Vector2f(20.f, 200.f));
    right_racket.setFillColor(color_racket);
    right_racket.setPosition(1220.f, yrr);

    //ball
    //float yb_direction = 1;
    //float xb_direction = 1;
    int b_direction = rand() % 90 + 45;
    if (rand() % 2 == 0)
    {
        b_direction = rand() % 90 + 225;
    }
    float b_speed = 1;
    float xb = 620;
    float yb = 450;
    sf::CircleShape ball(20.f);
    ball.setFillColor(color_racket);
    ball.setPosition(xb, yb);

    //variable to the pause
    bool pause = false;

    //left score
    sf::Text left_score;
    left_score.setFont(font);
    int l_score = 0;
    left_score.setCharacterSize(150);
    left_score.setFillColor(color_racket);
    left_score.setPosition(450.f, 30.f);

    //right score
    sf::Text right_score;
    right_score.setFont(font);
    int r_score = 0;
    right_score.setCharacterSize(150);
    right_score.setFillColor(color_racket);
    right_score.setPosition(650.f, 30.f);

    sf::Music game_music;

    game_music.openFromFile("game_music.ogg");
    game_music.setVolume(75);

    game_music.play();

    sf::Music goal;

    goal.openFromFile("goal.wav");
    goal.setVolume(80);

    //main loop
    while (window.isOpen())
    {
        // reaction to events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            if (event.type == sf::Event::KeyPressed)
            {
                if (event.key.code == sf::Keyboard::Escape)
                {
                    /* we can do:
                        if (pause == false) 
                        {
                            pause = true;
                        }
                        else
                        {
                            pause = false;
                        }
                    */

                    //but this is smarter:
                    pause = !pause;
                }
            }
        }

        mouse_position = sf::Mouse::getPosition(window);

        //pause hover
        if ((mouse_position.x >= pause_button_r.left) && (mouse_position.y >= pause_button_r.top) && (mouse_position.x <= pause_button_r.left + pause_button_r.width) && (mouse_position.y <= pause_button_r.top + pause_button_r.height))
        {
            pause_button.setFillColor(hover_grey);

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                pause = false;
            }
        }
        else
        {
            pause_button.setFillColor(grey_buttons);
        }

        //continue hover
        if ((mouse_position.x >= b_continue_r.left) && (mouse_position.y >= b_continue_r.top) && (mouse_position.x <= b_continue_r.left + b_continue_r.width) && (mouse_position.y <= b_continue_r.top + b_continue_r.height))
        {
            b_continue.setFillColor(hover_grey);

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                pause = false;
            }
        }
        else
        {
            b_continue.setFillColor(grey_buttons);
        }

        //new_game hover
        if ((mouse_position.x >= b_new_game_r.left) && (mouse_position.y >= b_new_game_r.top) && (mouse_position.x <= b_new_game_r.left + b_new_game_r.width) && (mouse_position.y <= b_new_game_r.top + b_new_game_r.height))
        {
            b_new_game.setFillColor(hover_grey);

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                l_score = 0;
                r_score = 0;
                xb = 620;
                yb = 450;
                pause = false;
                b_speed = 0;
                clocks.restart();
            }
        }
        else
        {
            b_new_game.setFillColor(grey_buttons);
        }

        //end hover
        if ((mouse_position.x >= b_end_r.left) && (mouse_position.y >= b_end_r.top) && (mouse_position.x <= b_end_r.left + b_end_r.width) && (mouse_position.y <= b_end_r.top + b_end_r.height))
        {
            b_end.setFillColor(hover_grey);

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                window.close();
            }
        }
        else
        {
            b_end.setFillColor(grey_buttons);
        }

        if (!pause) // pause == false
        {
            // reaction to W
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            {
                if (ylr >= 30)
                {
                    ylr = ylr - 2;
                    left_racket.setPosition(40.f, ylr);
                }
            }

            // reaction to S
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            {
                if (ylr <= 730)
                {
                    ylr = ylr + 2;
                    left_racket.setPosition(40.f, ylr);
                }
            }

            // reaction to UP
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            {
                if (yrr >= 30)
                {
                    yrr = yrr - 2;
                    right_racket.setPosition(1220.f, yrr);
                }
            }

            // reaction to DOWN
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            {
                if (yrr <= 730)
                {
                    yrr = yrr + 2;
                    right_racket.setPosition(1220.f, yrr);
                }
            }
        }

        //ball movement in y axis
        if ((yb <= 30) || (yb >= 890))
        {
            //when ball touch borders, he appear in center of window
            if (yb < 30)
            {
                yb = 30;
            }

            if (yb > 890)
            {
                yb = 890;
            }

            //yb_direction = -yb_direction;
            b_direction = 180 - b_direction;
            if (b_direction < 0)
            {
                b_direction = b_direction + 360;
            }
        }

        //when ball touch left border, againt appear
        if ((xb <= 2) || (xb >= 1238))
        {
            if (xb <= 2)
            {
                r_score++;
            }
            else
            {
                l_score++;
            }

            xb = 620;
            yb = 450;
            goal.play();

            if (rand() % 2 == 0)
            {
                b_direction = rand() % 90 + 225;
            }
            else
            {
                b_direction = rand() % 90 + 45;
            }

            b_speed = 0;
            clocks.restart();
        }

        //stop ball for 2 sec and after go
        if ((clocks.getElapsedTime() >= sf::milliseconds(2000)) && (b_speed == 0))
        {
            b_speed = 1;
        }

        //bounce from the left bat
        if ((xb <= 60) && (yb >= ylr) && (yb <= (ylr + 200)))
        {
            if (xb < 60)
            {
                xb = 61;
            }
            //xb_direction = -xb_direction;
            //yb_direction = -yb_direction;
            b_direction = 360 - b_direction;
            jumping.setVolume(25);
            jumping.play();
            if (b_speed < 3)
            {
                b_speed = b_speed + 0.20;
            }
        }

        //bounce from the right bat
        if ((xb >= 1180) && (yb >= yrr) && (yb <= (yrr + 200)))
        {
            if (xb > 1180)
            {
                xb = 1179;
            }

            //xb_direction = -xb_direction;
            //yb_direction = -yb_direction;
            b_direction = 360 - b_direction;
            jumping.setVolume(25);
            jumping.play();
            if (b_speed < 3)
            {
                b_speed = b_speed + 0.20;
            }
        }

        //ball movement
        if (pause == false)
        {
            yb = yb - b_speed * cos((b_direction * PI) / 180);
            xb = xb + b_speed * sin((b_direction * PI) / 180);
            ball.setPosition(xb, yb);
        }

        //drawing to the window
        window.clear(sf::Color::Black);
        window.draw(top_bar);
        window.draw(bottom_bar);
        window.draw(net);
        window.draw(left_racket);
        window.draw(right_racket);
        window.draw(ball);

        if (l_score >= 10)
        {
            left_score.setString(to_string(l_score));
        }
        else
        {
            left_score.setString(" " + to_string(l_score));
        }
        window.draw(left_score);

        if (r_score <= 9)
        {
            right_score.setString("  " + to_string(r_score));
        }

        else
        {
            right_score.setString(to_string(r_score));
        }
        window.draw(right_score);

        if (pause == true)
        {
            window.draw(dark_background);
            window.draw(pause_button);
            window.draw(line_pause);
            window.draw(line_two_pause);
            window.draw(b_continue);
            window.draw(b_new_game);
            window.draw(b_end);
            window.draw(t_continue);
            window.draw(t_new_game);
            window.draw(t_end);
        }

        window.display();
    }

    return 0;
}